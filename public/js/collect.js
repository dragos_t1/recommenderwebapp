
var xmlhttp;
var url = "http://localhost:8080/recommender/rest/api/add-preference";
if (window.XMLHttpRequest)
{
	// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
}
else
{	
	// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}


document.addEventListener('DOMContentLoaded',function(){
	var recommender_user = getCookie("recommender_user");
	if(recommender_user == "")
	{
		var random_string = Math.random().toString(36).substring(10);
		setCookie("recommender_user", random_string, 1000);
        recommender_user = random_string;
	}

    if(typeof recommenderApiKey == 'undefined')
        recommenderApiKey = "";

    if(typeof recommenderApiSecret == 'undefined')
        recommenderApiSecret = "";

    if(typeof recommenderCategory == 'undefined')
        recommenderCategory = "none";

    if(typeof recommenderName == 'undefined')
		recommenderName = "generic";
	
	if(typeof recommenderPhoto == 'undefined')
		recommenderPhoto = "none";
		
	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");

    recommenderCategory = encodeURIComponent(recommenderCategory);
    recommenderName     = encodeURIComponent(recommenderName);
    recommenderPhoto    = encodeURIComponent(recommenderPhoto);
	
	var post_url = "url="+document.URL
                    +"&user_id="+recommender_user
                    +"&category="+recommenderCategory
                    +"&name="+recommenderName
                    +"&photo="+recommenderPhoto
                    +"&appKey="+recommenderApiKey
                    +"&appSecret="+recommenderApiSecret;
	
	xmlhttp.send(post_url);
});