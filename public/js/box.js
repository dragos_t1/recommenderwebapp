
var xmlhttp;
var content = "<style>"+
"#recommender-recommendations {"+
"    list-style-type: none;"+
"    overflow: hidden;"+
"}"+
"#recommender-recommendations #recommender-recommend {"+
"    display:inline-block;"+
"    width:200px;"+
"    text-align: center;"+
"}"+
"#recommender-recommendations img {"+
"    display:block;"+
"    margin:auto;"+
"}"+
"</style>";

if (window.XMLHttpRequest)
{
	// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
}
else
{	
	// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

document.addEventListener('DOMContentLoaded',function(){
	var recommender_user = getCookie("recommender_user");
	if(recommender_user == "")
	{
		var random_string = Math.random().toString(36).substring(10);
		setCookie("recommender_user", random_string, 1000);
        recommender_user = random_string;
	}
	
	if(typeof recommenderApiKey == 'undefined')
		recommenderApiKey = "";
	
	if(typeof recommenderApiSecret == 'undefined')
        recommenderApiSecret = "";

    if(typeof recommenderNumberOfItems == 'undefined')
        recommenderNumberOfItems = 1;

	var get_url = "http://localhost:8080/recommender/rest/api/recommend"
                    +"?apiKey="+recommenderApiKey
                    +"&apiSecret="+recommenderApiSecret
                    +"&toUser="+recommender_user
                    +"&howMany="+recommenderNumberOfItems;

	
	xmlhttp.open("GET", get_url, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send();

	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var resp = JSON.parse(xmlhttp.responseText);

            content += "<div id='recommender-recommendations' >";
			for(var i in resp){
                content += '<div id="recommender-recommend">'
                                +'<a href="'+resp[i].url+'">'
                                    +'<img src="'+resp[i].photo_url+'" style="width:150px; height:150px" />'
                                    +resp[i].item_name
                                +'</a>'
                            +'</div>';
			}
			content += "</div>";

			document.getElementById("recommender-box").innerHTML=content;
        }
	}
});