@extends('layout.main')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-11 col-md-offset-1 col-sm-12">
            <h1 class="page-title">Test</h1>
        </div>
    </div>

    <div id="recommender-box"></div>

</div>

<script>
    var recommenderApiKey = 'TEST';
    var recommenderApiSecret = 'TEST';

    var recommenderCategory = 'automobile';
    var recommenderName = 'item2';
    var recommenderPhoto = 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQdWU51VMKb1vZBZENSVHEXsg03CNV6WNpjaWyGZu0phA1mjOcn';

    var recommenderNumberOfItems = 3;
</script>

<script type="text/javascript" src="{{ URL::asset('js/collect.js'); }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/box.js'); }}"></script>

@stop